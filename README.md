# Actividad 2

He usado esta [guía](https://medium.com/@momchilbattlenet/simple-guide-for-installing-tensorflow-gpu-version-on-wsl2-7e8aec2e3001)

Instalar la dependencia tensorflow con cuda desde la terminal de WSL, ya que me ha dado problemas y es como si no la hubiera instalado:

```
joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ source bin/activate

joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ source bin/activate
(mia-sca-act2) joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ pip list | grep "tensor*"
tensorboard                  2.15.2
tensorboard-data-server      0.7.2
tensorflow                   2.15.1
tensorflow-estimator         2.15.0
tensorflow-io-gcs-filesystem 0.37.0

[notice] A new release of pip is available: 23.2.1 -> 24.0
[notice] To update, run: pip install --upgrade pip

(mia-sca-act2) joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ python -c "import tensorflow as tf; print(tf.config.list_physical_devices('GPU'))"
2024-05-25 16:59:56.944634: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered
2024-05-25 16:59:56.944691: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered
2024-05-25 16:59:56.945326: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
2024-05-25 16:59:56.949586: I tensorflow/core/platform/cpu_feature_guard.cc:182] This TensorFlow binary is optimized to use available CPU instructions in performance-critical operations.
To enable the following instructions: AVX2 FMA, in other operations, rebuild TensorFlow with the appropriate compiler flags.
2024-05-25 16:59:57.787461: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT
2024-05-25 16:59:58.944825: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
Your kernel may have been built without NUMA support.
2024-05-25 16:59:59.022633: W tensorflow/core/common_runtime/gpu/gpu_device.cc:2256] Cannot dlopen some GPU libraries. Please make sure the missing libraries mentioned above are installed properly if you would like to use GPU. Follow the guide at https://www.tensorflow.org/install/gpu for how to download and setup the required libraries for your platform.
Skipping registering GPU devices...
[]

(mia-sca-act2) joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ pip install tensorflow[and-cuda]
Requirement already satisfied: tensorflow[and-cuda] in ./lib/python3.10/site-packages (2.15.1)
Requirement already satisfied: absl-py>=1.0.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (2.1.0)
Requirement already satisfied: astunparse>=1.6.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (1.6.3)
Requirement already satisfied: flatbuffers>=23.5.26 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (24.3.25)
Requirement already satisfied: gast!=0.5.0,!=0.5.1,!=0.5.2,>=0.2.1 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (0.5.4)
Requirement already satisfied: google-pasta>=0.1.1 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (0.2.0)
Requirement already satisfied: h5py>=2.9.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (3.11.0)
Requirement already satisfied: libclang>=13.0.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (18.1.1)
Requirement already satisfied: ml-dtypes~=0.3.1 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (0.3.2)
Requirement already satisfied: numpy<2.0.0,>=1.23.5 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (1.26.4)
Requirement already satisfied: opt-einsum>=2.3.2 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (3.3.0)
Requirement already satisfied: packaging in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (24.0)
Requirement already satisfied: protobuf!=4.21.0,!=4.21.1,!=4.21.2,!=4.21.3,!=4.21.4,!=4.21.5,<5.0.0dev,>=3.20.3 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (4.25.3)
Requirement already satisfied: setuptools in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (68.2.0)
Requirement already satisfied: six>=1.12.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (1.16.0)
Requirement already satisfied: termcolor>=1.1.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (2.4.0)
Requirement already satisfied: typing-extensions>=3.6.6 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (4.12.0)
Requirement already satisfied: wrapt<1.15,>=1.11.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (1.14.1)
Requirement already satisfied: tensorflow-io-gcs-filesystem>=0.23.1 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (0.37.0)
Requirement already satisfied: grpcio<2.0,>=1.24.3 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (1.64.0)
Requirement already satisfied: tensorboard<2.16,>=2.15 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (2.15.2)
Requirement already satisfied: tensorflow-estimator<2.16,>=2.15.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (2.15.0)
Requirement already satisfied: keras<2.16,>=2.15.0 in ./lib/python3.10/site-packages (from tensorflow[and-cuda]) (2.15.0)
Collecting nvidia-cublas-cu12==12.2.5.6 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cublas-cu12==12.2.5.6 from https://files.pythonhosted.org/packages/b6/6a/e8cca34f85b18a0280e3a19faca1923f6a04e7d587e9d8e33bc295a52b6d/nvidia_cublas_cu12-12.2.5.6-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cublas_cu12-12.2.5.6-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-cuda-cupti-cu12==12.2.142 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cuda-cupti-cu12==12.2.142 from https://files.pythonhosted.org/packages/cf/27/7c2f33b4fbab658117fce3b029f44cce7886fc5a50f526a81b4b0436af02/nvidia_cuda_cupti_cu12-12.2.142-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cuda_cupti_cu12-12.2.142-py3-none-manylinux1_x86_64.whl.metadata (1.6 kB)
Collecting nvidia-cuda-nvcc-cu12==12.2.140 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cuda-nvcc-cu12==12.2.140 from https://files.pythonhosted.org/packages/a7/a5/1b48eeda9bdc3ac5bf00d84eca6f31b568ab3da9008f754bf1fdd98ee97b/nvidia_cuda_nvcc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cuda_nvcc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-cuda-nvrtc-cu12==12.2.140 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cuda-nvrtc-cu12==12.2.140 from https://files.pythonhosted.org/packages/cb/84/bb975ca63e3d4017824ad3d62e141db8492bc53cec6dc84705fb0650c22d/nvidia_cuda_nvrtc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cuda_nvrtc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-cuda-runtime-cu12==12.2.140 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cuda-runtime-cu12==12.2.140 from https://files.pythonhosted.org/packages/95/46/6361d45c7a6fe3b3bb8d5fa35eb43c1dcd12d14799a0dc6faef3d76eaf41/nvidia_cuda_runtime_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cuda_runtime_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-cudnn-cu12==8.9.4.25 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cudnn-cu12==8.9.4.25 from https://files.pythonhosted.org/packages/fa/d7/f46bd08337201ec875bda25e29fcb65d26bdc6d0be306dc33fc0b092faa6/nvidia_cudnn_cu12-8.9.4.25-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cudnn_cu12-8.9.4.25-py3-none-manylinux1_x86_64.whl.metadata (1.6 kB)
Collecting nvidia-cufft-cu12==11.0.8.103 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cufft-cu12==11.0.8.103 from https://files.pythonhosted.org/packages/8f/86/311c32926aa321ebc169d6c910a1ecd30339ae198014e0d4a7cf448f6ce9/nvidia_cufft_cu12-11.0.8.103-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cufft_cu12-11.0.8.103-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-curand-cu12==10.3.3.141 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-curand-cu12==10.3.3.141 from https://files.pythonhosted.org/packages/c1/2c/9677fd666335801f565505bf831891b6ac6f645fe9e7689acc7d09f9a7fb/nvidia_curand_cu12-10.3.3.141-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_curand_cu12-10.3.3.141-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Collecting nvidia-cusolver-cu12==11.5.2.141 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cusolver-cu12==11.5.2.141 from https://files.pythonhosted.org/packages/1f/af/29126e0bae18bad59fe4018c2e32fd5d7ec0aa354a4b1e08c051515a25d1/nvidia_cusolver_cu12-11.5.2.141-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cusolver_cu12-11.5.2.141-py3-none-manylinux1_x86_64.whl.metadata (1.6 kB)
Collecting nvidia-cusparse-cu12==12.1.2.141 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-cusparse-cu12==12.1.2.141 from https://files.pythonhosted.org/packages/a9/b4/21af603eba3a803276bbcbbc41d6cf3a20919a2807217084cacef13ec779/nvidia_cusparse_cu12-12.1.2.141-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_cusparse_cu12-12.1.2.141-py3-none-manylinux1_x86_64.whl.metadata (1.6 kB)
Collecting nvidia-nccl-cu12==2.16.5 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-nccl-cu12==2.16.5 from https://files.pythonhosted.org/packages/da/ca/2894fe71d9773405746268a4e9fddfb57643c4cf2ac3f38d528bfb3e8af0/nvidia_nccl_cu12-2.16.5-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_nccl_cu12-2.16.5-py3-none-manylinux1_x86_64.whl.metadata (1.8 kB)
Collecting nvidia-nvjitlink-cu12==12.2.140 (from tensorflow[and-cuda])
  Obtaining dependency information for nvidia-nvjitlink-cu12==12.2.140 from https://files.pythonhosted.org/packages/0a/f8/5193b57555cbeecfdb6ade643df0d4218cc6385485492b6e2f64ceae53bb/nvidia_nvjitlink_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata
  Downloading nvidia_nvjitlink_cu12-12.2.140-py3-none-manylinux1_x86_64.whl.metadata (1.5 kB)
Requirement already satisfied: wheel<1.0,>=0.23.0 in ./lib/python3.10/site-packages (from astunparse>=1.6.0->tensorflow[and-cuda]) (0.41.2)
Requirement already satisfied: google-auth<3,>=1.6.3 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2.29.0)
Requirement already satisfied: google-auth-oauthlib<2,>=0.5 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (1.2.0)
Requirement already satisfied: markdown>=2.6.8 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (3.6)
Requirement already satisfied: requests<3,>=2.21.0 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2.32.2)
Requirement already satisfied: tensorboard-data-server<0.8.0,>=0.7.0 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (0.7.2)
Requirement already satisfied: werkzeug>=1.0.1 in ./lib/python3.10/site-packages (from tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (3.0.3)
Requirement already satisfied: cachetools<6.0,>=2.0.0 in ./lib/python3.10/site-packages (from google-auth<3,>=1.6.3->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (5.3.3)
Requirement already satisfied: pyasn1-modules>=0.2.1 in ./lib/python3.10/site-packages (from google-auth<3,>=1.6.3->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (0.4.0)
Requirement already satisfied: rsa<5,>=3.1.4 in ./lib/python3.10/site-packages (from google-auth<3,>=1.6.3->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (4.9)
Requirement already satisfied: requests-oauthlib>=0.7.0 in ./lib/python3.10/site-packages (from google-auth-oauthlib<2,>=0.5->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2.0.0)
Requirement already satisfied: charset-normalizer<4,>=2 in ./lib/python3.10/site-packages (from requests<3,>=2.21.0->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (3.3.2)
Requirement already satisfied: idna<4,>=2.5 in ./lib/python3.10/site-packages (from requests<3,>=2.21.0->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (3.7)
Requirement already satisfied: urllib3<3,>=1.21.1 in ./lib/python3.10/site-packages (from requests<3,>=2.21.0->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2.2.1)
Requirement already satisfied: certifi>=2017.4.17 in ./lib/python3.10/site-packages (from requests<3,>=2.21.0->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2024.2.2)
Requirement already satisfied: MarkupSafe>=2.1.1 in ./lib/python3.10/site-packages (from werkzeug>=1.0.1->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (2.1.5)
Requirement already satisfied: pyasn1<0.7.0,>=0.4.6 in ./lib/python3.10/site-packages (from pyasn1-modules>=0.2.1->google-auth<3,>=1.6.3->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (0.6.0)
Requirement already satisfied: oauthlib>=3.0.0 in ./lib/python3.10/site-packages (from requests-oauthlib>=0.7.0->google-auth-oauthlib<2,>=0.5->tensorboard<2.16,>=2.15->tensorflow[and-cuda]) (3.2.2)
Downloading nvidia_cublas_cu12-12.2.5.6-py3-none-manylinux1_x86_64.whl (417.8 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 417.8/417.8 MB 12.4 MB/s eta 0:00:00
Downloading nvidia_cuda_cupti_cu12-12.2.142-py3-none-manylinux1_x86_64.whl (13.9 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 13.9/13.9 MB 70.0 MB/s eta 0:00:00
Downloading nvidia_cuda_nvcc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl (21.3 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 21.3/21.3 MB 68.7 MB/s eta 0:00:00
Downloading nvidia_cuda_nvrtc_cu12-12.2.140-py3-none-manylinux1_x86_64.whl (23.4 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 23.4/23.4 MB 61.3 MB/s eta 0:00:00
Downloading nvidia_cuda_runtime_cu12-12.2.140-py3-none-manylinux1_x86_64.whl (845 kB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 845.8/845.8 kB 61.4 MB/s eta 0:00:00
Downloading nvidia_cudnn_cu12-8.9.4.25-py3-none-manylinux1_x86_64.whl (720.1 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 720.1/720.1 MB 1.1 MB/s eta 0:00:00
Downloading nvidia_cufft_cu12-11.0.8.103-py3-none-manylinux1_x86_64.whl (98.6 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 98.6/98.6 MB 7.0 MB/s eta 0:00:00
Downloading nvidia_curand_cu12-10.3.3.141-py3-none-manylinux1_x86_64.whl (56.5 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 56.5/56.5 MB 7.9 MB/s eta 0:00:00
Downloading nvidia_cusolver_cu12-11.5.2.141-py3-none-manylinux1_x86_64.whl (124.9 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 124.9/124.9 MB 3.9 MB/s eta 0:00:00
Downloading nvidia_cusparse_cu12-12.1.2.141-py3-none-manylinux1_x86_64.whl (195.3 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 195.3/195.3 MB 5.2 MB/s eta 0:00:00
Downloading nvidia_nccl_cu12-2.16.5-py3-none-manylinux1_x86_64.whl (188.7 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 188.7/188.7 MB 7.5 MB/s eta 0:00:00
Downloading nvidia_nvjitlink_cu12-12.2.140-py3-none-manylinux1_x86_64.whl (20.2 MB)
   ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 20.2/20.2 MB 51.2 MB/s eta 0:00:00
Installing collected packages: nvidia-nvjitlink-cu12, nvidia-nccl-cu12, nvidia-curand-cu12, nvidia-cufft-cu12, nvidia-cuda-runtime-cu12, nvidia-cuda-nvrtc-cu12, nvidia-cuda-nvcc-cu12, nvidia-cuda-cupti-cu12, nvidia-cublas-cu12, nvidia-cusparse-cu12, nvidia-cudnn-cu12, nvidia-cusolver-cu12
Successfully installed nvidia-cublas-cu12-12.2.5.6 nvidia-cuda-cupti-cu12-12.2.142 nvidia-cuda-nvcc-cu12-12.2.140 nvidia-cuda-nvrtc-cu12-12.2.140 nvidia-cuda-runtime-cu12-12.2.140 nvidia-cudnn-cu12-8.9.4.25 nvidia-cufft-cu12-11.0.8.103 nvidia-curand-cu12-10.3.3.141 nvidia-cusolver-cu12-11.5.2.141 nvidia-cusparse-cu12-12.1.2.141 nvidia-nccl-cu12-2.16.5 nvidia-nvjitlink-cu12-12.2.140

[notice] A new release of pip is available: 23.2.1 -> 24.0
[notice] To update, run: pip install --upgrade pip

(mia-sca-act2) joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ pip install --upgrade pip
Requirement already satisfied: pip in ./lib/python3.10/site-packages (23.2.1)
Collecting pip
  Obtaining dependency information for pip from https://files.pythonhosted.org/packages/8a/6a/19e9fe04fca059ccf770861c7d5721ab4c2aebc539889e97c7977528a53b/pip-24.0-py3-none-any.whl.metadata
  Using cached pip-24.0-py3-none-any.whl.metadata (3.6 kB)
Using cached pip-24.0-py3-none-any.whl (2.1 MB)
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 23.2.1
    Uninstalling pip-23.2.1:
      Successfully uninstalled pip-23.2.1
Successfully installed pip-24.0

(mia-sca-act2) joninazio@JONINX-DESKTOP:~/.virtualenvs/mia-sca-act2$ python3 -c "import tensorflow as tf; print(tf.config.list_physical_devices('GPU'))"
2024-05-25 17:05:17.937153: E external/local_xla/xla/stream_executor/cuda/cuda_dnn.cc:9261] Unable to register cuDNN factory: Attempting to register factory for plugin cuDNN when one has already been registered
2024-05-25 17:05:17.937218: E external/local_xla/xla/stream_executor/cuda/cuda_fft.cc:607] Unable to register cuFFT factory: Attempting to register factory for plugin cuFFT when one has already been registered
2024-05-25 17:05:17.939019: E external/local_xla/xla/stream_executor/cuda/cuda_blas.cc:1515] Unable to register cuBLAS factory: Attempting to register factory for plugin cuBLAS when one has already been registered
2024-05-25 17:05:17.946913: I tensorflow/core/platform/cpu_feature_guard.cc:182] This TensorFlow binary is optimized to use available CPU instructions in performance-critical operations.
To enable the following instructions: AVX2 FMA, in other operations, rebuild TensorFlow with the appropriate compiler flags.
2024-05-25 17:05:18.818579: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT
2024-05-25 17:05:19.904498: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
Your kernel may have been built without NUMA support.
2024-05-25 17:05:19.997178: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
Your kernel may have been built without NUMA support.
2024-05-25 17:05:19.997246: I external/local_xla/xla/stream_executor/cuda/cuda_executor.cc:887] could not open file to read NUMA node: /sys/bus/pci/devices/0000:01:00.0/numa_node
Your kernel may have been built without NUMA support.
[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')]
```

Como ves, ya hay soporte para GPU.